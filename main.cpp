#include <iostream>
#include <memory>

using namespace std;

class Object
{
public:
    Object(const string& str)
    {
        cout << "Constructor " << str << endl;
	name = str;
    }

    Object()
    {
        cout << "Default constructor" << endl;

    }

    ~Object()
    {
        cout << "Destructor " << name << endl;
    }

    Object(const Object& rhs)
    {
        cout << "Copy constructor..." << endl;
    }

	string name;
};

void make_shared_example()
{
    cout << "Create smart_ptr using make_shared..." << endl;
    auto ptr_res1 = make_shared<Object>("make_shared");
    cout << "Create smart_ptr using make_shared: done." << endl;

    cout << "Create smart_ptr using new..." << endl;
    shared_ptr<Object> ptr_res2(new Object("new"));
    cout << "Create smart_ptr using new: done." << endl;
}

int main(){
	make_shared_example();
	cout << "C++ version: " << __cplusplus << endl;
	return 0;
}
